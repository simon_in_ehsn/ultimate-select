import React, { Component } from 'react';
import SelectBoxs from './components/SelectBoxs';

class ShopItem extends Component {
  constructor(props) {
    super(props);
    const { styleList } = this.props.product;
    this.state = {
      selectedAmount: "1",
      selectedType: styleList.length ? (!this.selectedType ? styleList[0].name : this.selectedType) : '',
	  selectedSize: styleList.length ? (!this.selectedSize ? (styleList[0].subStyle ? styleList[0].subStyle[0].name : ""): this.selectedSize) : ''
    };
  }

  typeChange = (e) => {
    const { value } = e.target;
    const { styleList } = this.props.product;

    // setState樣式後 也setState該樣式第一筆尺寸
    styleList.forEach(item => {
      if (value === item.name) {
        this.setState({
          ...this.state,
		  selectedType: value,
		  selectedSize: item && item.subStyle ? item.subStyle[0].name : "",
          selectedAmount: 1
		}, () => console.log(this.state));
      }
    });
  }

  sizeChange = (e) => {
    const { value } = e.target
    this.setState({
      ...this.state,
      selectedSize: value,
      selectedAmount: 1
    }, () => console.log(this.state));
  }

  amountChange = (e) => {
	const { value } = e.target;
    this.setState({
      ...this.state,
      selectedAmount: value
    }, () => console.log(this.state));
  }


  render() {

    // 當庫存量不足10 option的數值對應庫存量 否則庫存量大於10就顯示10
    const { product } = this.props;
    const {
      inStock,
      name,
    } = product;

    return (

      <li className="product-item">
        <h3>{name}</h3>
        <div className="select-block">

          <SelectBoxs
            selectData={this.state}
            renderData={product}
            typeChange={this.typeChange}
            sizeChange={this.sizeChange}
            amountChange={this.amountChange}
          />

        </div>

        <div className="btn-block">
          <button type="button"
            onClick={() => alert("已加進購物車")}
            disabled={inStock ? '' : 'disabled'}
          >放入購物車</button>
        </div>
      </li>



    )
  }

}
export default ShopItem;


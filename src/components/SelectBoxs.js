import React from 'react';
import SelectItem from './SelectItem';
const SelectBoxs = ({ renderData, selectData, typeChange, sizeChange, amountChange }) => {
	let { styleList, inStock } = renderData;
	let {
		selectedAmount,
		selectedSize,
		selectedType
	} = selectData;
	
	let typeData = [];
	let sizeData = [];
	let amountData = [];

	let option = [
		typeData,
		sizeData,
		amountData
	]

	let selected = [
		selectedType,
		selectedSize
	]
	let change = [
		typeChange,
		sizeChange
	]
	let data = {
		selected,
		option,
		change,
		inStock,
		index: 0
	}

	/*
		todo:
		既然變數命名
		selectedType與selectedSize須改為中性名詞
		typeData與sizeData也須改為中性名詞

		selectedType與typeData有相依關係 又代表第一條選單 在遞迴物件中順序又是陣列第一筆
		selectedSize與sizeData有相依關係 又代表第二條選單 在遞迴物件中順序又是陣列第二筆
		
		那之後改名為 selectedFirst、FirstData與selectedSecond、SecondData....etc

	*/
	const renderSelect = (specData, data, select = { selected: [], onChange: [], data: [] }) => {
		let { index } = data;
		if (specData && (specData.length || specData.subStyle)) {
			const specStyle = (specData.subStyle && specData.subStyle.length) ? specData.subStyle : specData;

			select.data[index] = [];

			// 第一個下拉選單陣列
			let option = specStyle && specStyle.reduce((accumlator, currentValue) => {
				accumlator.push(currentValue.name);
				return accumlator;
			}, []);
			data.option[index].push(...option)
			select.data[index].push(...option)

			select.selected.push(data.selected[index])
			select.onChange.push(data.change[index])
			console.log(index)
			// 篩選出 商品資料 與 當前樣式option 對應的資料 以取得尺寸
			data.selected[index] = data.selected[index] || (specStyle && specStyle[0] && specStyle[0].name);
			const selectedStyle = specStyle && specStyle.find(item => {
				return data.selected[index] === item.name;
			});

			data.inStock = selectedStyle.inStock;
			data.index = data.index + 1

			// 把select抽到方法前面 return 給變數
			select = renderSelect(selectedStyle, data, select)

		}
		else {
			let { inStock } = data;
			data.option[index] = [...Array(inStock < 10 ? inStock : 10)].reduce((
				accumlator,
				currentValue,
				currentIndex
			) => {
				currentIndex > 0 && accumlator.push(currentIndex + 1);
				return accumlator;
			}, [1]);
			amountData = data.option[index]
			select.data[index] = [...amountData]
			select.selected.push(selectedAmount)
			select.onChange.push(amountChange)
		}

		return select;

	}


	let select = renderSelect(styleList, data)

	return (
		<div>
			{/* {
				typeData.length ?
					<SelectItem
						selectedValue={selectedType}
						onChange={e => typeChange(e)}
						data={typeData}
					/> : null
			}
			{
				sizeData.length ?
					<SelectItem
						selectedValue={selectedSize}
						onChange={e => sizeChange(e)}
						data={sizeData}
					/> : null
			}
			{
				amountData.length ?
					<SelectItem
						selectedValue={selectedAmount}
						onChange={e => amountChange(e)}
						data={amountData}
					/> : null
			} */}

			{
				select.selected.map((item, index) => {
					return <SelectItem
						selectedValue={select.selected[index]}
						onChange={e => select.onChange[index](e)}
						data={select.data[index]}
						key={index}
					/>
				})
			}
		</div>
	);
}

export default SelectBoxs;

import React from 'react';
import SelectItem from './SelectItem';
const SelectBoxs = ({ renderData, selectData, typeChange, sizeChange, amountChange }) => {
	let { styleList } = renderData;
	let {
		selectedAmount,
		selectedSize,
		selectedType
	} = selectData;

	let typeData = [];
	let sizeData = [];

	let optionData = [
		typeData,
		sizeData
	]
	let selectedData = [
		selectedType,
		selectedSize
	]
	

	let inStock = [renderData.inStock];

	let centralized = {
		selectedData,
		optionData,
		inStock,
	}


	const renderSelect = (specData, complex,index) => {

		if (specData && (specData.length || specData.subStyle)) {
			const specStyle = (specData.subStyle && specData.subStyle.length) ? specData.subStyle : specData;

			// 第一個下拉選單陣列
			let data = specStyle && specStyle.reduce((accumlator, currentValue) => {
				accumlator.push(currentValue.name);
				return accumlator;
			}, []);
			complex.optionData[index].push(...data)

			// 篩選出 商品資料 與 當前樣式option 對應的資料 以取得尺寸
			complex.selectedData[index] = complex.selectedData[index] || (specStyle && specStyle[0] && specStyle[0].name);
			const selectedStyle = specStyle && specStyle.find(item => {
				return complex.selectedData[index] === item.name;
			});
			console.log(selectedStyle)
			centralized.inStock[0] = selectedStyle.inStock;
			index = index+1
			renderSelect(selectedStyle,complex,index)
		}
		// return renderSelect(selectedStyle,complex,x)
	}

	renderSelect( styleList, centralized, 0 )

	// renderSelect(
	// 	styleList,
	// 	typeData,
	// 	selectedType,
	// 	inStock
	// )

	/*
	傳入物件
	物件內有{inStock:0}
	
	*/


	/* 
		53~84行為原本rnder的邏輯, 若要復原
		將16~43行給註解或刪除, 89行給註解或刪除 ,53~84解除註解 
		13行改成 let inStock = 0; 即可
	*/


	// if (styleList.length) {

	// 	// 第一個下拉選單陣列
	// 	typeData = styleList && styleList.reduce((accumlator, currentValue) => {
	// 		accumlator.push(currentValue.name);
	// 		return accumlator;
	// 	}, []);
	// 	// 篩選出 商品資料 與 當前樣式option 對應的資料 以取得尺寸
	// 	selectedType = selectedType || (styleList && styleList[0] && styleList[0].name);
	// 	const selectedStyle = styleList && styleList.find(item => {
	// 		return selectedType === item.name;
	// 	});
	// 	console.log(selectedStyle)
	// 	inStock = selectedStyle.inStock;

	// 	if (selectedStyle.subStyle && selectedStyle.subStyle.length) {
	// 		const subStyleList = selectedStyle.subStyle;

	// 		// 第二個下拉選單陣列
	// 		sizeData = subStyleList && subStyleList.reduce(function (accumlator, currentValue) {
	// 			accumlator.push(currentValue.name);
	// 			return accumlator;
	// 		}, []);
	// 		// 篩選出第二個資料與商品物件比對 以渲染第三個下拉選單陣列
	// 		selectedSize = selectedSize || (subStyleList && subStyleList[0] && subStyleList[0].name);
	// 		const selectedSubStyle = subStyleList.find(item => {
	// 			return selectedSize === item.name;
	// 		});
	// 		inStock = selectedSubStyle.inStock;
	// 	}

	// }




	inStock = inStock[0]


	const amountData = [...Array(inStock < 10 ? inStock : 10)].reduce((
		accumlator,
		currentValue,
		currentIndex
	) => {
		currentIndex > 0 && accumlator.push(currentIndex + 1);
		return accumlator;
	}, [1]);

	/* 	Todo: 將onChange寫在SelectBox內? 那外部接口傳入的參數就沒有methods了嗎?

		const onChange = (e, callback) => {
		callback && callback({ ...selectData, [e.target.name]: e.target.value })
	 }
	*/


	return (
		<div>
			{
				typeData.length ?
					<SelectItem
						selectedValue={selectedType}
						onChange={e => typeChange(e)}
						data={typeData}
					/> : null
			}
			{
				sizeData.length ?
					<SelectItem
						selectedValue={selectedSize}
						onChange={e => sizeChange(e)}
						data={sizeData}
					/> : null
			}
			{
				amountData ?
					<SelectItem
						selectedValue={selectedAmount}
						onChange={e => amountChange(e)}
						data={amountData}
					/> : null
			}


	
		</div>
	);
}

export default SelectBoxs;
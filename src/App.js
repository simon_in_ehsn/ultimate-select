import React, { Component } from 'react';
import shopJson from './api/shoppingJson';
import ShopItem from './ShopItem';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			cart: [],
			shopJson
		};
	}
	render() {
		return (
			<div className="App">
				<div className="App-wrap">

					{shopJson.map((item, index) => {
						return <ShopItem
							product={item}
							key={index}
						/>
					})}
				</div>
			</div>
		);
	}
}

export default App;

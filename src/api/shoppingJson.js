let shoppingJson = [
    {
      id: "155684",
      name: "創新傳統-炒鍋",
      price: 750,
      img: "https://images-cn.ssl-images-amazon.com/images/I/31xmrQBB2cL._SX300_QL70_.jpg",
      styleList: [],
      inStock: 5,
    },
    {
      id: "516222",
      name: "特級廚師-平底鍋",
      price: 660,
      img: "https://my-best.tw/wp-content/uploads/2017/09/%E9%87%91%E5%B1%AC%E6%8A%8A%E6%89%8B-300x200.jpg",
      styleList: [
        {
          id: "1136_0",
          name: "有鉚釘",
          inStock: 3
        },
        {
          name: "無鉚釘",
          id: "1423_0",
          inStock: 7, 
        }
      ],
      inStock: 35
    },
    {
      id: "805967",
      name: "匠心獨具-平底鍋",
      price: 730,
      img: "https://www.qiang100.com/data/upload/ueditor/20180605/5b15feac60188.png_resize_300_200.jpg",
      styleList: [
        {
          name: "鋁",
          id: "1084_8",
          inStock: 25,
          subStyle: [
            {
              id: "1914_0",
              name: "S",
              inStock: 7
            },
            {
              id: "1138_1",
              name: "M",
              inStock: 18
            }
          ]
        },
        {
          name: "304不銹鋼",
          id: "1224_8",
          inStock: 32,
          subStyle: [
            {
              id: "1138_1",
              name: "M",
              inStock: 12
            },
            {
              id: "1238_0",
              name: "L",
              inStock: 20
            }
          ]
        }
      ],
      inStock: 57,
    }
  ]
  export default shoppingJson;
  
